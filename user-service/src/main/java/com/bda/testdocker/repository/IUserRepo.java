package com.bda.testdocker.repository;

import com.bda.testdocker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepo extends JpaRepository <User, Long> {
}
