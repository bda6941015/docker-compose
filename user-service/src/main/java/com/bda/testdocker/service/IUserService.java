package com.bda.testdocker.service;

import com.bda.testdocker.model.User;

import java.util.List;

public interface IUserService {
    void createUser(User user);
    List<User> getAllUsers();
    void deleteById(Long id);
}
