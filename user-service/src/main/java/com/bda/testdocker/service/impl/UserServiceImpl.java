package com.bda.testdocker.service.impl;

import com.bda.testdocker.model.User;
import com.bda.testdocker.repository.IUserRepo;
import com.bda.testdocker.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor

public class UserServiceImpl implements IUserService {
    private final IUserRepo userRepo;


    @Override
    public void createUser(User user) {
        userRepo.save(user);

    }

    @Override
    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public void deleteById(Long id) {
        userRepo.deleteById(id);
    }
}
